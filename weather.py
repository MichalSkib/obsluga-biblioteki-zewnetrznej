from datetime import datetime
import requests
import sys


class WeatherForecast:

    BASE_URL = 'http://api.weatherapi.com/v1/history.json'

    def __init__(self, api_key, date=str(datetime.today().date())):
        self.api_key = api_key
        self.date = date
        self.data = self.get_data()
        self.weather = self.weather_info()

    def get_data(self):
        data_from_file = self.check_file(file_name='results.txt')
        if data_from_file is None:
            req_url = f'{self.BASE_URL}?key={self.api_key}&q=Warsaw&dt={self.date}'
            r = requests.get(req_url)
            content = r.json()
            return content
        else:
            return data_from_file

    def weather_info(self):
        initial_dict = {}
        with open('results.txt') as fp:
            for line in fp.readlines():
                divided_line = line.split(',')

                date_ = divided_line[0]
                info_ = divided_line[1].replace('\n', '')
                initial_dict[date_] = {
                    'info': info_,
                }
            return initial_dict

    def get_temperature(self):
        avg_temp = self.data['forecast']['forecastday'][0]['day']['avgtemp_c']
        return avg_temp

    def get_rain_info(self, file_name='results.txt'):
        if self.check_file(file_name) is None:
            totalprecip_mm = float(self.data['forecast']['forecastday'][0]['day']['totalprecip_mm'])
            return self.get_rain_chance(totalprecip_mm)
        else:
            return self.data

    def get_rain_chance(self, totalprecip_mm):
        if totalprecip_mm > 0.0:
            return 'Będzie padać'
        elif totalprecip_mm == 0.0:
            return 'Nie będzie padać'
        return 'Nie wiem!'

    def check_file(self, file_name):
        with open(file_name) as fp:
            while True:
                line = fp.readline()
                if not line:
                    break
                if self.date == line.split(',')[0]:
                    info = line.split(',')[1]
                    return info

    def write_to_file(self, file_name='results.txt'):
        if self.check_file(file_name) is None:
            with open(file_name, 'a+') as fp:
                fp.write(self.date + ',' + self.get_rain_info(file_name) + '\n')

    def items(self):
        for date, value in self.weather.items():
            yield date, value['info']

    def __getitem__(self, index):
        return self.weather[index]['info']

    def __iter__(self):
        for date in self.weather.keys():
            yield date


if len(sys.argv) >= 3:
    wf = WeatherForecast(api_key=sys.argv[1], date=sys.argv[2])
else:
    wf = WeatherForecast(api_key=sys.argv[1])

wf.write_to_file()
# print(wf.get_rain_info().replace('\n', ''))
print("##########")
print(wf[wf.date])
print("##########")
for date in wf:
    print(date)
print("##########")
for i in wf.items():
    print(i)
